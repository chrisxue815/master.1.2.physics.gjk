﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RLGE.Physics.ParticleEffectEngine.Emitters;

namespace RLGE.Physics.ParticleEffectEngine.Effects
{
    public class ParticleEffect
    {
        public List<Emitter> Emitters { get; set; }
        public Vector3 Position { get; set; }
        public IParticleRenderer ParticleRenderer { get; set; }

        public ParticleEffect()
        {
            Emitters = new List<Emitter>();
        }

        public virtual List<Particle> Update(float elapsedSeconds)
        {
            var particles = new List<Particle>();

            foreach (var emitter in Emitters)
            {
                var p = emitter.Update(elapsedSeconds);
                if (p != null) particles.AddRange(p);
            }

            return particles;
        }
    }
}
