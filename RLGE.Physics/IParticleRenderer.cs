﻿namespace RLGE.Physics
{
    public interface IParticleRenderer
    {
        void Draw(Particle particle);
    }
}
