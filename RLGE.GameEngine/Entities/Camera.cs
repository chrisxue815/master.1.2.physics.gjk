﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace RLGE.GameEngine.Entities
{
    public class Camera : GameEntity
    {
        public float FieldOfView { get; set; }
        public float ZNear { get; set; }
        public float ZFar { get; set; }

        public Matrix View { get; set; }
        public Matrix Projection { get; set; }

        private Vector3 TargetPosition { get; set; }

        private bool Following { get; set; }
        private GameEntity Target { get; set; }
        private Vector3 Offset { get; set; }

        private int PreviousX { get; set; }
        private int PreviousY { get; set; }
        private bool WasLeftPressed { get; set; }
        private bool WasRightPressed { get; set; }
        private int PreviousScrollWheel { get; set; }

        public Camera(BasicGame game)
            : base(game)
        {
            FieldOfView = MathHelper.PiOver4;
            ZNear = 0.1f;
            ZFar = 10000.0f;
        }

        public override void Initialize()
        {
            var viewport = Game.Graphics.GraphicsDevice.Viewport;
            var aspectRatio = Game.Graphics.GraphicsDevice.Viewport.AspectRatio;
            Projection = Matrix.CreateOrthographic(1920, 1080, ZNear, ZFar);
            Projection = Matrix.CreatePerspectiveFieldOfView(FieldOfView, aspectRatio, ZNear, ZFar);

            if (Following && Target != null) TargetPosition = Target.Position;
            View = Matrix.CreateLookAt(Position, TargetPosition, Vector3.Up);
        }

        public override void Update()
        {
            base.Update();

            var mouseState = Mouse.GetState();
            var x = mouseState.X;
            var y = mouseState.Y;
            var leftPressed = mouseState.LeftButton == ButtonState.Pressed;
            var rightPressed = mouseState.RightButton == ButtonState.Pressed;
            var scrollWheel = mouseState.ScrollWheelValue;

            var xOffset = x - PreviousX;
            var yOffset = y - PreviousY;
            var wheelOffset = scrollWheel - PreviousScrollWheel;

            if (leftPressed)
            {
                const float rotationSpeed = -0.01f;
                var yAngle = rotationSpeed * yOffset;
                var xAngle = rotationSpeed * xOffset;

                var rotation = Matrix.CreateRotationY(xAngle);

                var zenith = Math.Acos(Vector3.Dot(Offset, Vector3.Up) / Offset.Length());
                var newZenith = zenith + yAngle;

                if (newZenith > 0 && newZenith < Math.PI)
                {
                    var right = Vector3.Cross(Vector3.Up, Offset);
                    right.Normalize();

                    rotation *= Matrix.CreateFromAxisAngle(right, yAngle);
                }

                Offset = Vector3.Transform(Offset, rotation);
            }

            if (wheelOffset != 0)
            {
                const float zoomingSpeed = 0.01f;
                var offsetDirection = Vector3.Normalize(Offset);
                Offset -= zoomingSpeed * wheelOffset * offsetDirection;
            }

            if (Following && Target != null) TargetPosition = Target.Position;
            Position = TargetPosition + Offset;
            View = Matrix.CreateLookAt(Position, TargetPosition, Vector3.Up);

            PreviousX = x;
            PreviousY = y;
            WasLeftPressed = leftPressed;
            WasRightPressed = rightPressed;
            PreviousScrollWheel = scrollWheel;
        }

        public void LookAt(GameEntity target, Vector3 cameraOffset)
        {
            Following = true;
            Target = target;
            Offset = cameraOffset;
            Position = target.Position + cameraOffset;
        }

        public void LookAt(Vector3 cameraPosition, Vector3 targetPosition)
        {
            Following = false;
            Position = cameraPosition;
            TargetPosition = targetPosition;
            Offset = cameraPosition - targetPosition;
        }
    }
}
