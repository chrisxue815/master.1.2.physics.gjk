﻿using System;
using System.Runtime.Remoting.Channels;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RLGE.GameEngine;
using RLGE.GameEngine.VertexTypes;

namespace RLGE.Physics.Demo
{
    public class GJK : GameObject
    {
        private Vector3[] RawVertices { get; set; }
        private VertexBuffer VertexBuffer { get; set; }
        private BasicEffect BasicEffect { get; set; }
        private Model SphereModel { get; set; }
        private Model CubeModel { get; set; }
        private Vector3 Closest { get; set; }
        private Vector3 MousePos { get; set; }

        public GJK(BasicGame game)
            : base(game)
        {
        }

        public override void LoadContent()
        {
            RawVertices = new[]
            {
                new Vector3(0, 100, 0),
                new Vector3(130f, -80f, 0),
                new Vector3(-100f, -210f, 0)
            };

            var vertices = new[]
            {
                new VertexPosition(0, 100, 0),
                new VertexPosition(130f, -80f, 0),
                new VertexPosition(-100f, -210f, 0)
            };

            VertexBuffer = new VertexBuffer(Game.GraphicsDevice, typeof(VertexPosition), 3, BufferUsage.None);
            VertexBuffer.SetData(vertices);

            SphereModel = Game.Content.Load<Model>("models/sphere");
            CubeModel = Game.Content.Load<Model>("models/cube");

            BasicEffect = new BasicEffect(Game.GraphicsDevice);
        }

        public override void Update()
        {
            var mouseState = Mouse.GetState();
            MousePos = new Vector3(mouseState.X - 960, 540 - mouseState.Y, 0);

            var a = RawVertices[0];
            var b = RawVertices[1];
            var c = RawVertices[2];
            var x = MousePos;

            var ax = x - a;
            var bx = x - b;
            var cx = x - c;
            var ab = b - a;
            var ba = -ab;
            var ac = c - a;
            var ca = -ac;
            var bc = c - b;
            var cb = -bc;
            var abn = Vector3.Normalize(Vector3.Cross(Vector3.Cross(bc, ba), ba));
            var bcn = Vector3.Normalize(Vector3.Cross(Vector3.Cross(ca, cb), cb));
            var acn = Vector3.Normalize(Vector3.Cross(Vector3.Cross(ab, ac), ac));
            var xab = Vector3.Dot(ax, ab);
            var xac = Vector3.Dot(ax, ac);
            var xbc = Vector3.Dot(bx, bc);
            var xba = Vector3.Dot(bx, ba);
            var xca = Vector3.Dot(cx, ca);
            var xcb = Vector3.Dot(cx, cb);

            if (xab <= 0 && xac <= 0)
            {
                Closest = a;
            }
            else if (xbc <= 0 && xba <= 0)
            {
                Closest = b;
            }
            else if (xca <= 0 && xcb <= 0)
            {
                Closest = c;
            }
            else if (Vector3.Dot(abn, bx) >= 0 && xab >= 0 && xba >= 0)
            {
                Closest = x - Vector3.Dot(ax, abn) * abn;
            }
            else if (Vector3.Dot(bcn, cx) >= 0 && xbc >= 0 && xcb >= 0)
            {
                Closest = x - Vector3.Dot(bx, bcn) * bcn;
            }
            else if (Vector3.Dot(acn, ax) >= 0 && xac >= 0 && xca >= 0)
            {
                Closest = x - Vector3.Dot(cx, acn) * acn;
            }
        }

        public override void Draw()
        {
            BasicEffect.World = Matrix.Identity;
            BasicEffect.View = Game.Camera.View;
            BasicEffect.Projection = Game.Camera.Projection;
            BasicEffect.DiffuseColor = Color.Blue.ToVector3();

            var rasterizerState1 = new RasterizerState { CullMode = CullMode.None };
            Game.GraphicsDevice.RasterizerState = rasterizerState1;

            Game.GraphicsDevice.SetVertexBuffer(VertexBuffer);

            foreach (var pass in BasicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();

                Game.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, 1);
            }

            foreach (var mesh in SphereModel.Meshes)
            {
                foreach (BasicEffect basicEffect in mesh.Effects)
                {
                    basicEffect.World = Matrix.CreateScale(10) * Matrix.CreateTranslation(MousePos.X, MousePos.Y, 0);
                    basicEffect.View = Game.Camera.View;
                    basicEffect.Projection = Game.Camera.Projection;
                    basicEffect.DiffuseColor = Color.Blue.ToVector3();
                }
                mesh.Draw();
            }

            foreach (var mesh in SphereModel.Meshes)
            {
                foreach (BasicEffect basicEffect in mesh.Effects)
                {
                    basicEffect.World = Matrix.CreateScale(10) * Matrix.CreateTranslation(Closest.X, Closest.Y, 0);
                    basicEffect.View = Game.Camera.View;
                    basicEffect.Projection = Game.Camera.Projection;
                    basicEffect.DiffuseColor = Color.Red.ToVector3();
                }
                mesh.Draw();
            }

            return;

            var direction = MousePos - Closest;

            var angle = (float)Math.Acos(Vector3.Dot(direction, Vector3.Forward) / direction.Length());
            var axis = Vector3.Cross(Vector3.Forward, direction);
            axis.Normalize();
            var orientation = Quaternion.CreateFromAxisAngle(axis, angle);

            foreach (var mesh in SphereModel.Meshes)
            {
                foreach (BasicEffect basicEffect in mesh.Effects)
                {
                    basicEffect.World = Matrix.CreateTranslation(new Vector3(0, 0, -0.5f)) *
                        Matrix.CreateScale(10f, 10f, 80f) *
                        Matrix.CreateFromQuaternion(orientation) *
                        Matrix.CreateTranslation(Closest);
                    basicEffect.View = Game.Camera.View;
                    basicEffect.Projection = Game.Camera.Projection;
                    basicEffect.DiffuseColor = Color.Red.ToVector3();
                }
                mesh.Draw();
            }
        }
    }
}
