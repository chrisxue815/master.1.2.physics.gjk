﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RLGE.Physics;

namespace RLGE.GameEngine.Renderers
{
    public class ParticleEffectRenderer : GameObject, IParticleRenderer
    {
        private Model Model { get; set; }
        private Texture2D Texture { get; set; }

        public ParticleEffectRenderer(BasicGame game)
            : base(game)
        {
        }

        public override void LoadContent()
        {
            Model = Game.Content.Load<Model>("models/sphere");
            Texture = Game.Content.Load<Texture2D>("textures/Water001");
        }

        public void Draw(Particle particle)
        {
            /*
            VertexPositionTexture[]
            verts ={new VertexPositionTexture(new Vector3(Position.X,   y,   0), new Vector2(0.0f, 0.0f)),
            new VertexPositionTexture(new Vector3(x+w, y,   0), new Vector2(1.0f, 0.0f)),
            new VertexPositionTexture(new Vector3(x+w, y+h, 0), new Vector2(1.0f, 1.0f)),
            new VertexPositionTexture(new Vector3(x,   y+h, 0), new Vector2(0.0f, 1.0f))};

            var basicEffect = new BasicEffect(Game.GraphicsDevice);
            basicEffect.Alpha = 1.0f;
            basicEffect.TextureEnabled = true;
            basicEffect.Texture = Texture;
            basicEffect.EnableDefaultLighting();
            basicEffect.LightingEnabled = true;
            basicEffect.World = Matrix.CreateScale(0.05f) * Matrix.CreateTranslation(particle.Position);
            basicEffect.View = Game.Camera.View;
            basicEffect.Projection = Game.Camera.Projection;
            Game.GraphicsDevice.SamplerStates[0].AddressU = TextureAddressMode.Wrap;
            Game.GraphicsDevice.SamplerStates[0].AddressV = TextureAddressMode.Wrap;
            Game.GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            foreach (EffectPass pass in ParticleEffect.CurrentTechnique.Passes)
            {
                Game.GraphicsDevice.DrawUserPrimitives<VertexPositionTexture>(
                            PrimitiveType.TriangleFan,
                            verts, 0, 2);

                pass.Apply();
            }

            continue;*/
            foreach (var mesh in Model.Meshes)
            {
                foreach (BasicEffect basicEffect in mesh.Effects)
                {
                    basicEffect.World = Matrix.CreateScale(0.05f) * Matrix.CreateTranslation(particle.Position);
                    basicEffect.View = Game.Camera.View;
                    basicEffect.Projection = Game.Camera.Projection;
                    basicEffect.DiffuseColor = particle.DiffuseColor;
                    basicEffect.EnableDefaultLighting();
                }
                mesh.Draw();
            }
        }
    }
}
