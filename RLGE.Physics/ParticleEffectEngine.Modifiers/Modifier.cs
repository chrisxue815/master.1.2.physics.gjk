﻿namespace RLGE.Physics.ParticleEffectEngine.Modifiers
{
    public class Modifier
    {
        public virtual void Update(float elapsedSeconds, Particle particles)
        {
        }
    }
}
