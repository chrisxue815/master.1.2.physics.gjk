﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RLGE.Physics.RigidBodyEngine.Entities
{
    public class Tetrahedron : RigidBody
    {
        public Vector3[] Vertices { get; set; }
        public List<short>[] Edges { get; set; }
        public List<short>[] Faces { get; set; }
        public float Scale { get; set; }

        public Tetrahedron()
        {
            Vertices = new Vector3[4]
            {
                new Vector3(0, (float)Math.Sqrt(3f/8), 0),
                new Vector3(-(float)Math.Sqrt(1f/3), -(float)Math.Sqrt(1f/24), 0),
                new Vector3((float)Math.Sqrt(1f/12), -(float)Math.Sqrt(1f/24), 0.5f),
                new Vector3((float)Math.Sqrt(1f/12), -(float)Math.Sqrt(1f/24), -0.5f)
            };

            Edges = new List<short>[4]
            {
                new List<short> { 1, 2, 3 },
                new List<short> { 2, 3, 0 },
                new List<short> { 3, 0, 1 },
                new List<short> { 0, 1, 2 }
            };

            Faces = new List<short>[4]
            {
                new List<short> { 0, 1, 2 },
                new List<short> { 2, 1, 3 },
                new List<short> { 2, 3, 0 },
                new List<short> { 0, 3, 1 },
            };

            Scale = 5;

            var c = Mass * Scale * 2 / 5;

            InertiaTensor = new Matrix(
                c, 0, 0, 0,
                0, c, 0, 0,
                0, 0, c, 0,
                0, 0, 0, 1);

            InverseInertiaTensor = Matrix.Invert(InertiaTensor);
        }

        public override bool HandleCollision(Particle particle)
        {
            return false;
        }

        public Vector3 SupportingPoint(Vector3 direction)
        {
            var curr = 0;

            for (;;)
            {
                var vertex = Vertices[curr];
                var adjacents = Edges[curr];
                var max = 0f;
                for (var i = 0; i < adjacents.Count; i++)
                {
                    var adjacent = Vertices[adjacents[i]];
                    var dot = Vector3.Dot(vertex, adjacent);
                    if (dot > 0 && dot > max)
                    {
                        max = dot;
                        curr = i;
                    }
                }

                if (max <= 0)
                {
                    break;
                }
            }

            return Vertices[curr];
        }
    }
}
