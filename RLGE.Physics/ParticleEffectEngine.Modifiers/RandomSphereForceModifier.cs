﻿using System;
using Microsoft.Xna.Framework;

namespace RLGE.Physics.ParticleEffectEngine.Modifiers
{
    public class RandomSphereForceModifier : Modifier
    {
        public Vector3 Force { get; set; }
        private Random Random { get; set; }

        public RandomSphereForceModifier(Vector3 force)
        {
            Force = force;
            Random = new Random();
        }

        public override void Update(float elapsedSeconds, Particle particle)
        {
            Force += new Vector3(RandomExt.NextReal(0.1f), RandomExt.NextReal(0.1f), RandomExt.NextReal(0.1f));
            particle.NetForce += Force;
        }
    }
}
