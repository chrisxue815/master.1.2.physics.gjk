﻿using Microsoft.Xna.Framework;
using RLGE.Physics.ParticleEffectEngine.Emitters;
using RLGE.Physics.ParticleEffectEngine.Modifiers;

namespace RLGE.Physics.ParticleEffectEngine.Effects
{
    public class BasicSpringEffect : ParticleEffect
    {
        public Vector3 Direction { get; set; }

        public BasicSpringEffect(Vector3 position, Vector3 direction)
        {
            Position = position;
            Direction = direction;

            var coneEmitter = new ConeEmitter(this)
            {
                Direction = direction,
                Angle = 0.3f,
                MinimumTriggerPeriod = 0.01f,
                ReleaseSpeed = 10,
                ReleaseDiffuseColor = new Vector3(1, 0, 0)
            };

            coneEmitter.Modifiers.Add(ConstantForceModifier.Gravity);
            //coneEmitter.Modifiers.Add(new RandomSphereForceModifier(new Vector3(1, 0.5f, 0)));
            coneEmitter.Modifiers.Add(new DiffuseColorModifier());

            Emitters.Add(coneEmitter);
        }
    }
}
