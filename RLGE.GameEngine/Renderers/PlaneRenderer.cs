﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RLGE.GameEngine.Entities;
using RLGE.Physics;
using Plane = RLGE.Physics.RigidBodyEngine.Entities.Plane;

namespace RLGE.GameEngine.Renderers
{
    public class PlaneRenderer : GameObject, IParticleRenderer
    {
        private Model Model { get; set; }
        private Ground Ground { get; set; }

        public PlaneRenderer(BasicGame game)
            : base(game)
        {
            Ground = new Ground(game);
        }

        public override void LoadContent()
        {
            Ground.LoadContent();
            Model = Game.Content.Load<Model>("models/cube");
        }

        public void Draw(Particle particle)
        {
            var plane = particle as Plane;

            if (plane == null) return;

            Ground.Position = plane.Position;
            Ground.Draw();

            return;

            foreach (var mesh in Model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.World = Matrix.CreateScale(20, 0.1f, 20) * Matrix.CreateTranslation(plane.PointOnPlane);
                    effect.View = Game.Camera.View;
                    effect.Projection = Game.Camera.Projection;
                    effect.DiffuseColor = new Vector3(0.3f);
                    //ParticleEffect.EnableDefaultLighting();
                }
                mesh.Draw();
            }
        }
    }
}
