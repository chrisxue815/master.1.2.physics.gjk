﻿using RLGE.GameEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace RLGE.Physics.Demo
{
    public class Game1 : BasicGame
    {
        //private EntityManager EntityManager { get; set; }
        private Controller Controller { get; set; }
        private GJK GJK { get; set; }

        public Game1()
        {
            Graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            Graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            Graphics.ApplyChanges();
            SetBorderlessWindow();

            //EntityManager = new EntityManager(this);
            //GameObjects.Add(EntityManager);

            GJK = new GJK(this);
            GameObjects.Add(GJK);

            Controller = new Controller(this);
            GameObjects.Add(Controller);

            Camera.LookAt(new Vector3(0, 0, 1000), Vector3.Zero);
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        protected override void Update()
        {
            // Allows the game to exit
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Exit();
            }
        }

        protected override void Draw()
        {
        }
    }
}
