﻿namespace RLGE.GameEngine
{
    public class GameObject
    {
        protected BasicGame Game { get; set; }

        public GameObject(BasicGame game)
        {
            Game = game;
        }

        public virtual void Initialize()
        {
        }

        public virtual void LoadContent()
        {
        }

        public virtual void UnloadContent()
        {
        }

        public virtual void Update()
        {
        }

        public virtual void Draw()
        {
        }
    }
}
