﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RLGE.GameEngine.Renderers
{
    public class Arrow : GameEntity
    {
        private Model SphereModel { get; set; }
        private Model CubeModel { get; set; }

        public Arrow(BasicGame game)
            : base(game)
        {
        }

        public override void LoadContent()
        {
            SphereModel = Game.Content.Load<Model>("models/sphere");
            CubeModel = Game.Content.Load<Model>("models/cube");
        }

        public void Draw(Vector3 position, Vector3 direction)
        {
            var angle = (float)Math.Acos(Vector3.Dot(direction, Vector3.Forward) / direction.Length());
            var axis = Vector3.Cross(Vector3.Forward, direction);
            axis.Normalize();
            var orientation = Quaternion.CreateFromAxisAngle(axis, angle);

            foreach (var mesh in SphereModel.Meshes)
            {
                foreach (BasicEffect basicEffect in mesh.Effects)
                {
                    basicEffect.World = Matrix.CreateScale(0.05f) * Matrix.CreateTranslation(position);
                    basicEffect.View = Game.Camera.View;
                    basicEffect.Projection = Game.Camera.Projection;
                    basicEffect.DiffuseColor = Color.Red.ToVector3();
                }
                mesh.Draw();
            }

            foreach (var mesh in CubeModel.Meshes)
            {
                foreach (BasicEffect basicEffect in mesh.Effects)
                {
                    basicEffect.World = Matrix.CreateTranslation(new Vector3(0, 0, -0.5f)) *
                        Matrix.CreateScale(0.01f, 0.01f, 1f) *
                        Matrix.CreateFromQuaternion(orientation) *
                        Matrix.CreateTranslation(position);
                    basicEffect.View = Game.Camera.View;
                    basicEffect.Projection = Game.Camera.Projection;
                    basicEffect.DiffuseColor = Color.Red.ToVector3();
                }
                mesh.Draw();
            }
        }
    }
}
