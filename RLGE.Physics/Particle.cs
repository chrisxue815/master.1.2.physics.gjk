﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RLGE.Physics.ParticleEffectEngine.Modifiers;

namespace RLGE.Physics
{
    public class Particle
    {
        public float Mass { get; set; }
        public Vector3 Position { get; set; }
        public Vector3 LinearVelocity { get; set; }

        public Vector3 NetForce { get; set; }

        public Vector3 PreviousPosition { get; set; }

        public Vector3 DiffuseColor { get; set; }

        public bool IsAlive { get; set; }
        public bool Kinematics { get; set; }

        public List<Modifier> Modifiers { get; set; }
        public IParticleRenderer Renderer { get; set; }

        public Particle()
        {
            Modifiers = new List<Modifier>();
            Kinematics = false;
        }

        public virtual void Update(float elapsedSeconds)
        {
            foreach (var modifier in Modifiers)
            {
                modifier.Update(elapsedSeconds, this);
            }
        }
    }
}
