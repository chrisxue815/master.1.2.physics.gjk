﻿using System.Collections.Generic;

namespace RLGE.GameEngine
{
    public class GameObjectManager : GameObject
    {
        protected List<GameObject> GameObjects { get; set; }

        public GameObjectManager(BasicGame game) : base(game)
        {
            GameObjects = new List<GameObject>();
        }

        public override void Initialize()
        {
            foreach (var gameComponent in GameObjects)
            {
                gameComponent.Initialize();
            }
        }

        public override void LoadContent()
        {
            foreach (var gameComponent in GameObjects)
            {
                gameComponent.Initialize();
            }
        }

        public override void UnloadContent()
        {
            foreach (var gameComponent in GameObjects)
            {
                gameComponent.UnloadContent();
            }
        }

        public override void Update()
        {
            foreach (var gameComponent in GameObjects)
            {
                gameComponent.Update();
            }
        }

        public override void Draw()
        {
            foreach (var gameComponent in GameObjects)
            {
                gameComponent.Draw();
            }
        }
    }
}
