﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RLGE.Physics;
using RLGE.Physics.RigidBodyEngine.Entities;

namespace RLGE.GameEngine.Renderers
{
    public class CubeModelRenderer : GameObject, IParticleRenderer
    {
        private Model Model { get; set; }

        public CubeModelRenderer(BasicGame game)
            : base(game)
        {
        }

        public override void LoadContent()
        {
            Model = Game.Content.Load<Model>("models/cube");
        }

        public void Draw(Particle particle)
        {
            var cube = particle as Cube;

            if (cube == null) return;

            var world = Matrix.CreateFromQuaternion(cube.Orientation);
            world *= Matrix.CreateScale(cube.Width, cube.Height, cube.Depth);
            world *= Matrix.CreateTranslation(cube.Position);

            foreach (var mesh in Model.Meshes)
            {
                foreach (BasicEffect basicEffect in mesh.Effects)
                {
                    basicEffect.World = world;
                    basicEffect.View = Game.Camera.View;
                    basicEffect.Projection = Game.Camera.Projection;
                    basicEffect.DiffuseColor = particle.DiffuseColor;
                    basicEffect.EnableDefaultLighting();
                }
                mesh.Draw();
            }
        }
    }
}
