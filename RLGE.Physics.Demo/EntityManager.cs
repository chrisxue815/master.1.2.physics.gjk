﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;
using RLGE.GameEngine;
using RLGE.GameEngine.Entities;
using RLGE.GameEngine.Renderers;
using Microsoft.Xna.Framework;
using RLGE.Physics.RigidBodyEngine;
using RLGE.Physics.RigidBodyEngine.Entities;

namespace RLGE.Physics.Demo
{
    public class EntityManager : GameObjectManager
    {
        private Space Space { get; set; }
        private ParticleEffectRenderer ParticleEffectRenderer { get; set; }
        private PlaneRenderer PlaneRenderer { get; set; }
        private CubeRenderer CubeRenderer { get; set; }
        private CubeModelRenderer CubeModelRenderer { get; set; }
        private CubeSkeletonRenderer CubeSkeletonRenderer { get; set; }
        private CubeColorfulRenderer CubeColorfulRenderer { get; set; }
        private GridLineGround GridLineGround { get; set; }
        private List<RigidBody> RigidBodies { get; set; }
        private Cube Cube { get; set; }
        private Arrow Arrow { get; set; }

        public EntityManager(BasicGame game)
            : base(game)
        {
            Space = new Space();
            Space.GlobalForce = new Vector3(0, -9.8f, 0);
            ParticleEffectRenderer = new ParticleEffectRenderer(game);
            PlaneRenderer = new PlaneRenderer(game);
            CubeRenderer = new CubeRenderer(game);
            CubeModelRenderer = new CubeModelRenderer(game);
            CubeSkeletonRenderer = new CubeSkeletonRenderer(game);
            CubeColorfulRenderer = new CubeColorfulRenderer(game);
            Arrow = new Arrow(game);

            RigidBodies = new List<RigidBody>();

            var tx = RandomExt.NextReal();
            var ty = RandomExt.NextReal();
            var tz = RandomExt.NextReal();

            var baseCube = new Cube()
            {
                Position = new Vector3(0, 0.5f, 0),
                //AngularVelocity = new Vector3(tx, ty, tz),
                DiffuseColor = Color.Blue.ToVector3(),
                Renderer = CubeSkeletonRenderer,
                Kinematics = true
            };

            Space.Add(baseCube);

            GridLineGround = new GridLineGround(game);

            game.Camera.LookAt(new Vector3(8, 8, 10), Vector3.Zero);
        }

        public override void LoadContent()
        {
            ParticleEffectRenderer.LoadContent();
            PlaneRenderer.LoadContent();
            CubeRenderer.LoadContent();
            CubeModelRenderer.LoadContent();
            CubeSkeletonRenderer.LoadContent();
            CubeColorfulRenderer.LoadContent();
            GridLineGround.LoadContent();
            Arrow.LoadContent();

            base.LoadContent();
        }

        private float generationCountdown = 1;

        public override void Update()
        {
            //var generatingRigidBody = RandomExt.NextBool(Game.ElapsedSeconds / 1f);
            var generatingRigidBody = false;

            generationCountdown -= Game.ElapsedSeconds;

            if (generationCountdown <= 0)
            {
                generationCountdown += 1;
                generatingRigidBody = true;
            }
            
            if (generatingRigidBody)
            {
                var x = RandomExt.NextFloat(-2, 2);
                var z = RandomExt.NextFloat(-2, 2);
                var tx = RandomExt.NextReal();
                var ty = RandomExt.NextReal();
                var tz = RandomExt.NextReal();

                Cube = new Cube()
                {
                    Position = new Vector3(0, 5, 0),
                    DiffuseColor = Color.Green.ToVector3(),
                    Renderer = CubeSkeletonRenderer,
                    AngularVelocity = new Vector3(tx, ty, tz)
                };
                RigidBodies.Add(Cube);
                Space.Add(Cube);
            }

            Space.Update(Game.ElapsedSeconds);

            if (RigidBodies.Count > 1 || RigidBodies.Count > 0 && Game.Pause && Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                Space.Remove(RigidBodies[0]);
                RigidBodies.RemoveAt(0);
                Cube.Kinematics = false;
            }

            base.Update();
        }

        public override void Draw()
        {
            Space.Draw(Game.ElapsedSeconds);
            GridLineGround.Draw();

            if (Cube != null && Cube.Kinematics)
            {
                Arrow.Draw(Cube.CollisionPoint, Vector3.Up);
                Game.Pause = true;
            }
        }
    }
}
