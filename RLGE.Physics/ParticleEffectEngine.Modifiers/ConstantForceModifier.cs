﻿using Microsoft.Xna.Framework;

namespace RLGE.Physics.ParticleEffectEngine.Modifiers
{
    public class ConstantForceModifier : Modifier
    {
        public static ConstantForceModifier Gravity = new ConstantForceModifier(new Vector3(0, -9.8f, 0));

        public Vector3 Force { get; set; }

        public ConstantForceModifier(Vector3 force)
        {
            Force = force;
        }

        public override void Update(float elapsedSeconds, Particle particle)
        {
            particle.NetForce += Force;
        }
    }
}
