﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RLGE.GameEngine.VertexTypes;

namespace RLGE.GameEngine.Entities
{
    public class GridLineGround : GameEntity
    {
        private VertexBuffer VertexBuffer { get; set; }
        private IndexBuffer IndexBuffer { get; set; }
        private BasicEffect BasicEffect { get; set; }
        private int NumLines { get; set; }

        public GridLineGround(BasicGame game)
            : base(game)
        {
        }

        public override void LoadContent()
        {
            CreateBuffers();

            BasicEffect = new BasicEffect(Game.GraphicsDevice);
        }

        private void CreateBuffers()
        {
            var width = 20;
            var height = 20;
            var gridLength = 5;

            var right = width * gridLength / 2;
            var left = -right;
            var bottom = height * gridLength / 2;
            var top = -bottom;

            NumLines = width + height + 2;
            var numVertices = NumLines * 2;
            var vertices = new VertexPosition[numVertices];
            var iVertice = 0;

            for (var x = left; x <= right; x += gridLength)
            {
                vertices[iVertice++] = new VertexPosition(x, 0, top);
                vertices[iVertice++] = new VertexPosition(x, 0, bottom);
            }

            for (var y = top; y <= bottom; y += gridLength)
            {
                vertices[iVertice++] = new VertexPosition(left, 0, y);
                vertices[iVertice++] = new VertexPosition(right, 0, y);
            }

            VertexBuffer = new VertexBuffer(Game.GraphicsDevice, typeof(VertexPosition), vertices.Length, BufferUsage.None);
            VertexBuffer.SetData(vertices);
        }

        public override void Draw()
        {
            BasicEffect.World = Matrix.CreateTranslation(Position);
            BasicEffect.View = Game.Camera.View;
            BasicEffect.Projection = Game.Camera.Projection;
            BasicEffect.DiffuseColor = Color.DarkBlue.ToVector3();

            Game.GraphicsDevice.Indices = IndexBuffer;
            Game.GraphicsDevice.SetVertexBuffer(VertexBuffer);

            var rasterizerState1 = new RasterizerState { CullMode = CullMode.None };
            Game.GraphicsDevice.RasterizerState = rasterizerState1;

            foreach (var pass in BasicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();

                Game.GraphicsDevice.DrawPrimitives(PrimitiveType.LineList, 0, NumLines);
            }
        }
    }
}
