﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RLGE.Physics.ParticleEffectEngine.Effects;

namespace RLGE.Physics.ParticleEffectEngine.Emitters
{
    public class PointEmitter : Emitter
    {
        private Random Random { get; set; }

        public PointEmitter(ParticleEffect particleEffect)
            : base(particleEffect)
        {
            Random = new Random();
        }

        public override List<Particle> GenerateParticles()
        {
            var particles = new List<Particle>();

            var xAngle = (float)Random.NextDouble() * MathHelper.TwoPi;
            var yAngle = (float)Random.NextDouble() * MathHelper.TwoPi;
            var xRotation = Quaternion.CreateFromAxisAngle(Vector3.Right, xAngle);
            var direction = Vector3.Transform(Vector3.Forward, xRotation);
            var newY = Vector3.Transform(Vector3.Up, xRotation);
            var yRotation = Quaternion.CreateFromAxisAngle(newY, yAngle);
            direction = Vector3.Transform(direction, yRotation);
            direction.Normalize();

            var velocity = direction * ReleaseSpeed;

            var particle = new Particle()
            {
                Mass = 1,
                Position = Position,
                LinearVelocity = velocity,
                DiffuseColor = ReleaseDiffuseColor,
                Modifiers = Modifiers,
                Renderer = ParticleEffect.ParticleRenderer
            };

            particles.Add(particle);

            return particles;
        }
    }
}
