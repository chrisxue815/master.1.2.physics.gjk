﻿using System;
using System.Linq;

namespace RLGE.Physics
{
    public class RandomExt
    {
        private static Random Random { get; set; }

        static RandomExt()
        {
            Random = new Random();
        }

        public static float NextFloat()
        {
            return (float)Random.NextDouble();
        }

        public static float NextFloat(float range)
        {
            return (float)(Random.NextDouble() * range);
        }

        public static float NextFloat(float from, float to)
        {
            var range = to - from;
            var rand = (float)(Random.NextDouble() * range);
            return rand + from;
        }

        public static float NextReal()
        {
            return (float)(Random.NextDouble() * 2 - 1);
        }

        public static float NextReal(float range)
        {
            return (float)(Random.NextDouble() * 2 * range - range);
        }

        public static bool NextBool()
        {
            return Random.Next(2) == 1;
        }

        public static bool NextBool(float possibility)
        {
            return Random.NextDouble() < possibility;
        }

        public static int NextInt(float[] possibilities)
        {
            var rand = NextFloat();
            var accumulator = 0.0f;
            var i = 0;

            for (; i < possibilities.Count(); i++)
            {
                accumulator += possibilities[i];
                if (accumulator >= rand)
                {
                    break;
                }
            }

            return i;
        }

        public static T NextEnum<T>(T t, float[] possibilities)
        {
            var rand = NextInt(possibilities);
            return (T)Enum.GetValues(typeof(T)).GetValue(rand);
        }
    }
}
