﻿using Microsoft.Xna.Framework;

namespace RLGE.Physics.RigidBodyEngine.Entities
{
    public abstract class RigidBody : Particle
    {
        public Matrix InertiaTensor { get; set; }
        public Matrix InverseInertiaTensor { get; set; }
        public Quaternion Orientation { get; set; }
        public Vector3 AngularVelocity { get; set; }

        public Vector3 NetTorque { get; set; }

        public Quaternion PreviousOrientation { get; set; }

        public Vector3 CollisionPoint { get; set; }

        protected RigidBody()
        {
            Mass = 1;
        }

        public abstract bool HandleCollision(Particle particle);

        public virtual Vector3 SupportingPoint(Vector3 direction)
        {
            return Vector3.Zero;
        }

        public override void Update(float elapsedSeconds)
        {
            base.Update(elapsedSeconds);

            PreviousPosition = Position;
            PreviousOrientation = Orientation;

            var linearAcceleration = NetForce / Mass;
            LinearVelocity += linearAcceleration * elapsedSeconds;
            Position += LinearVelocity * elapsedSeconds;

            var angularAcceleration = Vector3.Transform(AngularVelocity, InertiaTensor);
            angularAcceleration = Vector3.Cross(AngularVelocity, angularAcceleration);
            var deltaAngularVelocity = Vector3.Transform(NetTorque - angularAcceleration, InverseInertiaTensor);
            deltaAngularVelocity *= elapsedSeconds;
            AngularVelocity += deltaAngularVelocity;

            if (AngularVelocity.Length() > 0)
            {
                var axis = Vector3.Normalize(AngularVelocity);
                var angle = AngularVelocity.Length() * elapsedSeconds;
                var rotation = Quaternion.CreateFromAxisAngle(axis, angle);
                Orientation *= rotation;
            }
        }
    }
}
