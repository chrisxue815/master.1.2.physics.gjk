﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RLGE.GameEngine.VertexTypes;
using RLGE.Physics;
using RLGE.Physics.RigidBodyEngine.Entities;

namespace RLGE.GameEngine.Renderers
{
    public class TetrahedronSkeletonRenderer : GameObject, IParticleRenderer
    {
        private Tetrahedron Tetrahedron { get; set; }

        private VertexPositionNormal[] Vertices { get; set; }
        private short[] Indices { get; set; }

        private VertexBuffer VertexBuffer { get; set; }
        private IndexBuffer IndexBuffer { get; set; }

        private BasicEffect BasicEffect { get; set; }
        private SpriteFont SpriteFont { get; set; }

        public TetrahedronSkeletonRenderer(BasicGame game, Tetrahedron tetrahedron)
            : base(game)
        {
            Tetrahedron = tetrahedron;
        }

        public override void LoadContent()
        {
            CreateBuffers();

            BasicEffect = new BasicEffect(Game.GraphicsDevice);

            SpriteFont = Game.Content.Load<SpriteFont>("fonts/monitor");
        }

        private void CreateBuffers()
        {
            Vertices = new VertexPositionNormal[4];

            for (var i = 0; i < 4; i++)
            {
                Vertices[i] = new VertexPositionNormal(Tetrahedron.Vertices[i]);
            }

            Indices = new short[12]
            {
                0, 1,
                0, 2,
                0, 3,
                1, 2,
                1, 3,
                2, 3,
            };

            VertexBuffer = new VertexBuffer(Game.GraphicsDevice, typeof(VertexPositionNormal), Vertices.Length, BufferUsage.None);
            VertexBuffer.SetData(Vertices);

            IndexBuffer = new IndexBuffer(Game.GraphicsDevice, IndexElementSize.SixteenBits, Indices.Length, BufferUsage.None);
            IndexBuffer.SetData(Indices);
        }

        public void Draw(Particle particle)
        {
            var world = Matrix.CreateFromQuaternion(Tetrahedron.Orientation);
            world *= Matrix.CreateScale(Tetrahedron.Scale);
            world *= Matrix.CreateTranslation(Tetrahedron.Position);

            BasicEffect.World = world;
            BasicEffect.View = Game.Camera.View;
            BasicEffect.Projection = Game.Camera.Projection;
            BasicEffect.DiffuseColor = particle.DiffuseColor;
            BasicEffect.EnableDefaultLighting();

            Game.GraphicsDevice.Indices = IndexBuffer;
            Game.GraphicsDevice.SetVertexBuffer(VertexBuffer);

            var rasterizerState1 = new RasterizerState { CullMode = CullMode.None };
            Game.GraphicsDevice.RasterizerState = rasterizerState1;

            foreach (var pass in BasicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();

                Game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.LineList, 0, 0, VertexBuffer.VertexCount, 0, 6);
            }

            var verbose = false;

            if (verbose)
            {
                //test
                var infoPos = new Vector2(10, 10);

                foreach (var vertex in Vertices)
                {
                    var v1 = vertex.Position;
                    var v2 = Vector3.Transform(v1, world);
                    var info =
                        string.Format(
                            "({0:+0.00;-0.00}, {1:+0.00;-0.00}, {2:+0.00;-0.00}) => ({3:+0.00;-0.00}, {4:+0.00;-0.00}, {5:+0.00;-0.00})",
                            v1.X, v1.Y, v1.Z, v2.X, v2.Y, v2.Z);

                    Game.SpriteBatch.DrawString(SpriteFont, info, infoPos, Color.Black);

                    infoPos += new Vector2(0, 20);
                }
            }
        }
    }
}
