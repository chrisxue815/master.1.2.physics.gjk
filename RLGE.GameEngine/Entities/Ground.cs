﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RLGE.GameEngine.VertexTypes;

namespace RLGE.GameEngine.Entities
{
    public class Ground : GameEntity
    {
        private VertexBuffer VertexBuffer { get; set; }
        private IndexBuffer IndexBuffer { get; set; }
        private BasicEffect BasicEffect { get; set; }
        private int NumTriangles { get; set; }

        public Ground(BasicGame game)
            : base(game)
        {
        }

        public override void LoadContent()
        {
            CreateBuffers();

            BasicEffect = new BasicEffect(Game.GraphicsDevice);
        }

        private void CreateBuffers()
        {
            var width = 20;
            var height = 20;
            var gridLength = 5;

            var right = width * gridLength / 2;
            var left = -right;
            var bottom = height * gridLength / 2;
            var top = -bottom;
            var numVertices = (width + 1) * (height + 1);
            var vertices = new VertexPosition[numVertices];
            var iVertice = 0;

            for (var y = top; y <= bottom; y += gridLength)
            {
                for (var x = left; x <= right; x += gridLength)
                {
                    vertices[iVertice++] = new VertexPosition(x, 0, y);
                }
            }

            VertexBuffer = new VertexBuffer(Game.GraphicsDevice, typeof(VertexPosition), vertices.Length, BufferUsage.None);
            VertexBuffer.SetData(vertices);

            var numSquares = width * height / 2 + width * height % 2;
            NumTriangles = numSquares * 2;
            var numIndices = NumTriangles * 3;
            var indices = new short[numIndices];
            var iIndex = 0;

            for (var j = 0; j < height; j++)
            {
                for (var i = 0; i < width; i++)
                {
                    if ((i + j) % 2 != 0)
                    {
                        continue;
                    }

                    var topLeft = (short)(j * (width + 1) + i);
                    var topRight = (short)(topLeft + 1);
                    var bottomLeft = (short)((j + 1) * (width + 1) + i);
                    var bottomRight = (short)(bottomLeft + 1);

                    indices[iIndex++] = topLeft;
                    indices[iIndex++] = topRight;
                    indices[iIndex++] = bottomRight;
                    indices[iIndex++] = bottomRight;
                    indices[iIndex++] = bottomLeft;
                    indices[iIndex++] = topLeft;
                }
            }

            IndexBuffer = new IndexBuffer(Game.GraphicsDevice, IndexElementSize.SixteenBits, indices.Length, BufferUsage.None);
            IndexBuffer.SetData(indices);
        }

        public override void Draw()
        {
            BasicEffect.World = Matrix.CreateTranslation(Position);
            BasicEffect.View = Game.Camera.View;
            BasicEffect.Projection = Game.Camera.Projection;
            BasicEffect.DiffuseColor = Color.DarkBlue.ToVector3();

            Game.GraphicsDevice.Indices = IndexBuffer;
            Game.GraphicsDevice.SetVertexBuffer(VertexBuffer);

            var rasterizerState1 = new RasterizerState { CullMode = CullMode.None };
            Game.GraphicsDevice.RasterizerState = rasterizerState1;

            foreach (var pass in BasicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();

                Game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, VertexBuffer.VertexCount, 0,
                    NumTriangles);
            }
        }
    }
}
