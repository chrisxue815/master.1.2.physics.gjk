﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RLGE.GameEngine.VertexTypes
{
    public struct VertexPositionNormal : IVertexType
    {
        public Vector3 Position;
        public Vector3 Normal;

        public static readonly VertexDeclaration Declaration = new VertexDeclaration
        (
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(sizeof(float) * 3, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0)
        );

        public VertexDeclaration VertexDeclaration { get { return Declaration; } }

        public VertexPositionNormal(Vector3 position, Color color, Vector3 normal)
        {
            Position = position;
            Normal = normal;
        }

        public VertexPositionNormal(Vector3 position, Color color)
        {
            Position = position;
            Normal = new Vector3();
        }

        public VertexPositionNormal(Vector3 position)
        {
            Position = position;
            Normal = new Vector3();
        }

        public VertexPositionNormal(float x, float y, float z)
        {
            Position = new Vector3(x, y, z);
            Normal = new Vector3();
        }

        public VertexPositionNormal(float x, float y, float z, Color color)
        {
            Position = new Vector3(x, y, z);
            Normal = new Vector3();
        }
    }
}
