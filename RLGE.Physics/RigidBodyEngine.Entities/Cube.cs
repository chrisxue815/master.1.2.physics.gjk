﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace RLGE.Physics.RigidBodyEngine.Entities
{
    public class Cube : RigidBody
    {
        public float Width { get; set; }
        public float Height { get; set; }
        public float Depth { get; set; }

        private Vector3[] Vertices { get; set; }
        public List<short>[] Edges { get; set; }
        public List<short>[] Faces { get; set; }

        public Matrix WorldTransform { get; set; }
        public Vector3[] WorldVertices { get; set; }

        public Vector3 Scale
        {
            get { return new Vector3(Width, Height, Depth); }
            set
            {
                Width = value.X;
                Height = value.Y;
                Depth = value.Z;
            }
        }

        public Cube(float width, float height, float depth)
            : this(new Vector3(width, height, depth))
        {
        }

        public Cube(float scale)
            : this(new Vector3(scale, scale, scale))
        {
        }

        public Cube()
            : this(new Vector3(1, 1, 1))
        {
        }

        public Cube(Vector3 scale)
        {
            Scale = scale;
            Orientation = Quaternion.Identity;
            Mass = 1;

            var wSqr = Width * Width;
            var hSqr = Height * Height;
            var dSqr = Depth * Depth;
            var c = Mass / 12;

            InertiaTensor = new Matrix(
                c * (hSqr + dSqr), 0, 0, 0,
                0, c * (wSqr + dSqr), 0, 0,
                0, 0, c * (wSqr * hSqr), 0,
                0, 0, 0, 1);

            InverseInertiaTensor = Matrix.Invert(InertiaTensor);

            Vertices = new[]
            {
                new Vector3(-0.5f, -0.5f, -0.5f),
                new Vector3(+0.5f, -0.5f, -0.5f),
                new Vector3(+0.5f, +0.5f, -0.5f),
                new Vector3(-0.5f, +0.5f, -0.5f),
                new Vector3(-0.5f, -0.5f, +0.5f),
                new Vector3(+0.5f, -0.5f, +0.5f),
                new Vector3(+0.5f, +0.5f, +0.5f),
                new Vector3(-0.5f, +0.5f, +0.5f)
            };

            WorldVertices = new Vector3[8];

            Edges = new[]
            {
                new List<short>{1,3,4},
                new List<short>{0,2,5},
                new List<short>{1,3,6},
                new List<short>{0,2,7},
                new List<short>{0,5,7},
                new List<short>{1,4,6},
                new List<short>{2,5,7},
                new List<short>{3,4,6}
            };

            Faces = new[]
            {
                new List<short>{0,1,2,3},
                new List<short>{4,5,6,7},
                new List<short>{0,1,5,4},
                new List<short>{2,3,7,6},
                new List<short>{1,2,6,5},
                new List<short>{0,3,7,6}
            };
        }

        public override void Update(float elapsedSeconds)
        {
            base.Update(elapsedSeconds);

            WorldTransform = Matrix.CreateFromQuaternion(Orientation);
            WorldTransform *= Matrix.CreateScale(Scale);
            WorldTransform *= Matrix.CreateTranslation(Position);

            for (var i = 0; i < Vertices.Length; i++)
            {
                WorldVertices[i] = Vector3.Transform(Vertices[i], WorldTransform);
            }
        }

        public override bool HandleCollision(Particle particle)
        {
            return false;
        }

        public override Vector3 SupportingPoint(Vector3 direction)
        {
            var curr = 0;

            for (; ; )
            {
                var vertex = WorldVertices[curr];
                var adjacents = Edges[curr];
                var max = 0f;
                for (var i = 0; i < adjacents.Count; i++)
                {
                    var adjacent = WorldVertices[adjacents[i]];
                    var edge = adjacent - vertex;
                    var dot = Vector3.Dot(edge, direction);
                    if (dot > 0 && dot > max)
                    {
                        max = dot;
                        curr = adjacents[i];
                    }
                }

                if (max <= 0)
                {
                    break;
                }
            }

            return WorldVertices[curr];
        }
    }
}
