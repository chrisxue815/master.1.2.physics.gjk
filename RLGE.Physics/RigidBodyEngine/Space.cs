using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RLGE.Physics.ParticleEffectEngine.Effects;
using RLGE.Physics.RigidBodyEngine.Entities;

namespace RLGE.Physics.RigidBodyEngine
{
    public class Space
    {
        public Vector3 GlobalForce { get; set; }
        public List<ParticleEffect> Effects { get; set; }
        private List<RigidBody> RigidBodies { get; set; }
        private List<Particle> Particles { get; set; }

        public Space()
        {
            Effects = new List<ParticleEffect>();
            RigidBodies = new List<RigidBody>();
            Particles = new List<Particle>();
        }

        public void Add(RigidBody rigidBody)
        {
            RigidBodies.Add(rigidBody);
        }

        public void Add(ParticleEffect particleEffect)
        {
            Effects.Add(particleEffect);
        }

        public void Remove(RigidBody rigidBody)
        {
            RigidBodies.Remove(rigidBody);
        }

        public void Remove(ParticleEffect particleEffect)
        {
            Effects.Remove(particleEffect);
        }

        public void Update(float elapsedSeconds)
        {
            UpdateRigidBodies(elapsedSeconds);
            UpdateParticles(elapsedSeconds);
        }

        private void UpdateRigidBodies(float elapsedSeconds)
        {
            foreach (var rigidBody in RigidBodies)
            {
                rigidBody.NetForce = rigidBody.Kinematics ? Vector3.Zero : GlobalForce;

                rigidBody.Update(elapsedSeconds);
            }

            for (var i = 0; i < RigidBodies.Count; i++)
            {
                var a = RigidBodies[i];

                for (var j = i + 1; j < RigidBodies.Count; j++)
                {
                    var b = RigidBodies[j];

                    var supportingPoint = b.SupportingPoint(Vector3.Down);

                    if (supportingPoint.Y < 1)
                    {
                        var c = 1;
                    }

                    if (supportingPoint.Y < 1 && supportingPoint.X >= -0.5 && supportingPoint.X <= 0.5
                        && supportingPoint.Z >= -0.5 && supportingPoint.Z <= 0.5)
                    {
                        b.Kinematics = true;
                        b.CollisionPoint = supportingPoint;
                    }
                }
            }
        }

        /*
        private bool GJK(RigidBody a, RigidBody b)
        {
            var simplices = new List<Vector3>();

            var direction = b.Position - a.Position;
            var p1 = RigidBody.SupportingPoint(a, b, direction);
            simplices.Add(p1);
            direction = -direction;

            for (; ; )
            {
                var last = RigidBody.SupportingPoint(a, b, direction);
                simplices.Add(last);
                var proj = Vector3.Dot(last, direction);

                if (proj <= 0)
                {
                    return false;
                }
                else if (ContainsOrigin(simplices, direction))
                {
                    return true;
                }
            }
        }
        */

        private void UpdateParticles(float elapsedSeconds)
        {
            foreach (var effect in Effects)
            {
                foreach (var emitter in effect.Emitters)
                {
                    var particles = emitter.Update(elapsedSeconds);
                    if (particles != null) Particles.AddRange(particles);
                }
            }

            foreach (var particle in Particles)
            {
                particle.Update(elapsedSeconds);

                particle.PreviousPosition = particle.Position;

                var a = particle.NetForce / particle.Mass;
                particle.LinearVelocity += a * elapsedSeconds;
                particle.Position += particle.LinearVelocity * elapsedSeconds;

                particle.NetForce = Vector3.Zero;

                var collisionDetected = false;

                do
                {
                    foreach (var rigidBody in RigidBodies)
                    {
                        collisionDetected = rigidBody.HandleCollision(particle);
                        if (collisionDetected) break;
                    }
                } while (collisionDetected);
            }
        }

        public void Draw(float elapsedSeconds)
        {
            foreach (var particle in Particles)
            {
                if (particle.Renderer != null) particle.Renderer.Draw(particle);
            }

            foreach (var rigidBody in RigidBodies)
            {
                if (rigidBody.Renderer != null) rigidBody.Renderer.Draw(rigidBody);
            }
        }
    }
}
