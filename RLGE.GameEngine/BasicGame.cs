using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RLGE.GameEngine.Entities;

namespace RLGE.GameEngine
{
    public class BasicGame : Game
    {
        public Camera Camera { get; set; }

        protected List<GameObject> GameObjects { get; set; }

        public float ElapsedSeconds { get; set; }
        public float TotalSeconds { get; set; }

        public GraphicsDeviceManager Graphics { get; set; }
        public SpriteBatch SpriteBatch { get; set; }

        public bool Pause { get; set; }
        public bool WasPause { get; set; }

        public BasicGame()
        {
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            GameObjects = new List<GameObject>();

            Camera = new Camera(this);
            Camera.LookAt(new Vector3(0, 0, -5), Vector3.Zero);
            GameObjects.Add(Camera);

            Pause = false;
            WasPause = false;
        }

        protected override void Initialize()
        {
            foreach (var gameObject in GameObjects)
            {
                gameObject.Initialize();
            }

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            foreach (var gameObject in GameObjects)
            {
                gameObject.LoadContent();
            }
        }

        protected override void UnloadContent()
        {
            foreach (var gameObject in GameObjects)
            {
                gameObject.UnloadContent();
            }
        }

        protected override void Update(GameTime gameTime)
        {
            if (Pause)
            {
                ElapsedSeconds = 0;
            }
            else
            {
                ElapsedSeconds = (float) gameTime.ElapsedGameTime.TotalSeconds;
                TotalSeconds = (float) gameTime.TotalGameTime.TotalSeconds;
            }

            Update();

            foreach (var gameObject in GameObjects)
            {
                gameObject.Update();
            }

            base.Update(gameTime);
        }

        protected virtual void Update()
        {
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Gray);

            SpriteBatch.Begin();

            var state = new DepthStencilState { DepthBufferEnable = true };
            GraphicsDevice.DepthStencilState = state;

            Draw();

            foreach (var gameObject in GameObjects)
            {
                gameObject.Draw();
            }

            SpriteBatch.End();

            base.Draw(gameTime);
        }

        protected virtual void Draw()
        {
        }

        protected void SetBorderlessWindow()
        {
            var hWnd = Window.Handle;
            var control = System.Windows.Forms.Control.FromHandle(hWnd);
            var form = control.FindForm();
            if (form == null) return;
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            form.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        }
    }
}
