﻿using System.Linq;
using Microsoft.Xna.Framework.Input;
using RLGE.GameEngine;

namespace RLGE.Physics.Demo
{
    public class Controller : GameObject
    {
        private Keys[] PreviousKeys { get; set; }

        public Controller(BasicGame game) : base(game)
        {
        }

        public override void Update()
        {
            var keyState = Keyboard.GetState();
            var keys = keyState.GetPressedKeys();

            foreach (var key in keys)
            {
                if (PreviousKeys.Contains(key))
                {
                    continue;
                }

                switch (key)
                {
                    case Keys.Space:
                        Game.Pause = !Game.Pause;
                        break;
                }
            }

            PreviousKeys = keys;
        }
    }
}
