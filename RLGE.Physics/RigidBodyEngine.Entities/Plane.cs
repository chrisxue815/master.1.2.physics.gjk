﻿using System;
using Microsoft.Xna.Framework;

namespace RLGE.Physics.RigidBodyEngine.Entities
{
    public class Plane : RigidBody
    {
        public Vector3 PointOnPlane { get; set; }
        public Vector3 Normal { get; set; }

        public override bool HandleCollision(Particle particle)
        {
            var collision = DetectCollision(particle);
            if (!collision.HasValue || particle.LinearVelocity == Vector3.Zero)
            {
                return false;
            }

            var collisionPoint = collision.Value;
            var movement = particle.Position - particle.PreviousPosition;
            var movementInNormalDir = Vector3.Dot(movement, Normal);

            const float restitutionInNormalDir = 0.8f;
            const float restitutionInTangentialDir = 0.9f;

            if (Math.Abs(movementInNormalDir) > 0.01f)
            {
                var postMovement = particle.Position - collisionPoint;
                var postMovementInNormalDir = Vector3.Dot(postMovement, Normal);

                particle.PreviousPosition = collisionPoint;
                particle.Position -= postMovementInNormalDir * 2 * Normal;

                var velocityMagnitude = Vector3.Dot(particle.LinearVelocity, Normal);
                var velocityInNormalDir = velocityMagnitude * Normal;
                var velocityInTangentialDir = particle.LinearVelocity - velocityInNormalDir;

                var newVelocityInNormalDir = -restitutionInNormalDir * velocityInNormalDir;
                var newVelocityInTangentialDir = restitutionInTangentialDir * velocityInTangentialDir;
                var newVelocity = newVelocityInNormalDir + newVelocityInTangentialDir;

                particle.LinearVelocity = newVelocity;
            }
            else
            {
                particle.Position -= movementInNormalDir * Normal;

                var velocityMagnitude = Vector3.Dot(particle.LinearVelocity, Normal);
                var velocityInNormalDir = velocityMagnitude * Normal;
                var velocityInTangentialDir = particle.LinearVelocity - velocityInNormalDir;

                var newVelocityInTangentialDir = restitutionInTangentialDir * velocityInTangentialDir;
                var newVelocity = newVelocityInTangentialDir;

                particle.LinearVelocity = newVelocity;
            }

            return true;
        }

        public Vector3? DetectCollision(Particle particle)
        {
            var p1 = particle.PreviousPosition;
            var p2 = particle.Position;
            var movement = p2 - p1;
            var movementInNormalDir = Vector3.Dot(movement, Normal);

            var ratio = Vector3.Dot(PointOnPlane - p1, Normal) / movementInNormalDir;

            if (ratio > 0 && ratio < 1)
            {
                ratio -= 0.01f;
                return p1 + ratio * movement;
            }
            else
            {
                return null;
            }
        }
    }
}
