﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RLGE.Physics.ParticleEffectEngine.Effects;

namespace RLGE.Physics.ParticleEffectEngine.Emitters
{
    public class ConeEmitter : Emitter
    {
        public Vector3 Direction { get; set; }
        public float Angle { get; set; }
        private Random Random { get; set; }

        public ConeEmitter(ParticleEffect particleEffect)
            : base(particleEffect)
        {
            Random = new Random();
        }

        public override List<Particle> GenerateParticles()
        {
            var particles = new List<Particle>();

            var m = Matrix.CreateFromAxisAngle(Vector3.Backward, Angle / 2);
            var a = Vector3.Transform(Direction, m);
            var b = (float)Random.NextDouble() * MathHelper.TwoPi;
            m = Matrix.CreateFromAxisAngle(Direction, b);
            var d = Vector3.Transform(a, m);

            var velocity = d * ReleaseSpeed;

            var particle = new Particle()
            {
                Mass = 1,
                Position = Position,
                LinearVelocity = velocity,
                DiffuseColor = ReleaseDiffuseColor,
                Modifiers = Modifiers,
                Renderer = ParticleEffect.ParticleRenderer
            };

            particles.Add(particle);

            return particles;
        }
    }
}
