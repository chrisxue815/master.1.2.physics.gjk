﻿using Microsoft.Xna.Framework;

namespace RLGE.GameEngine
{
    public class GameEntity : GameObject
    {
        public Vector3 Position { get; set; }
        public Quaternion Orientation { get; set; }

        public Vector3 InitialRight { get; set; }
        public Vector3 InitialUp { get; set; }
        public Vector3 InitialFront { get; set; }

        public GameEntity(BasicGame game) : base(game)
        {
        }
    }
}
